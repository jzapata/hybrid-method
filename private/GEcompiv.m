function [A,LIcols] = GEcompiv(A,tol)

% GEcompiv(A,tol) finds Linear Independent columns of a rectangular matrix A using complete
% pivoting. This function stops once all rows have been reduced or the
% maximum value in the remaining rows is less than a prescribed tolerance.

% INPUT:    A is a rectangular matrix of dimension m x n.
%           tol is a prescribed tolerance 

% OUTPUT:   A is the reduced matrix with zeros under pivot elements
%           LIcols is a list of column-indices 

[m, n] = size(A);

if (nargin < 2)
    tol = max(m,n)*eps(class(A))*norm(A,inf);
end

% Loop over the entire matrix.
i = 1;
j = 1;
LIcols = zeros(1,0);
while i <= m && j <= n
   % Find max value in matrix A(i:end,:)
        [p, ind] = max(abs(A(i:end, 1:end)), [], 'all', 'linear');
%         abs(A(i:end, 1:end))
        [row,col] = ind2sub(size(A(i:end, 1:end),1),ind);
        row = row + i - 1;
%         A(row,col)

   if p <= tol
       disp('Remaining elements are almost zero!!')
       break
   else
    % Remember column index
    LIcols = [LIcols col];

    % Swap rows
    A([i, row], 1:n) = A([row, i], 1:n);
        
    %% Gaussian Elimination
    % Divide the pivot row by the pivot element.
    A(i,1:n) = A(i,1:n)./A(i,col);
    % Subtract multiples of the pivot row from all the other rows.
    for k = i+1:m
     A(k,1:n) = A(k,1:n) - A(k,col).*A(i,1:n);
    end
    i = i + 1;
    j = j + 1;

   end
end
end
