function CheckSDPSolution(varX,varY,varL,varXmatsubs)
disp('!!!!!!')
[Sols,msolEigs,Sx,Sy,Sl,msolNote] = CheckMsolveOutputs(varX,varY,varL,varXmatsubs);


% % msolEigs
% msolEigsReal = [];
% for i=1:size(msolEigs,1)
%     %any(~isreal(msolEigs(i,:)))
%     if all(isreal(msolEigs(i,:)))
%         msolEigsReal = [msolEigsReal;msolEigs(i,:)];
%     end
% end
% msolEigs = msolEigsReal;


% if isempty(msolNote)
%     % msolEigs
%     msolEigs(abs(msolEigs)<10e-10) = 0;
%     PSDEigs=double(msolEigs)<0;
%     PSDEigs(all(msolEigs==0,2),:)=[]; 
%     PSDEigs = find(all(PSDEigs==0,2));
%     if ~isempty(PSDEigs)
%         NoteMSol = [NoteMSol;{'Success'}];
%         disp('Success!');

%     else
%         NoteMSol = [NoteMSol;{'NegEigs'}];
%     end
% else
%     if cellfun(@isequal, msolNote, {'Pos dim syst'})
%         NoteMSol = [NoteMSol;{'Pos dim syst'}];
%         disp('Positive Dimensional System!');
%     elseif cellfun(@isequal, msolNote, {'empty syst'})
%         NoteMSol = [NoteMSol;{'empty syst'}];
%         disp('Polynomial System has not solution!');
%     end
% end
