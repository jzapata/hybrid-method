function [Xnum,OutcomeP,MEPrimal] = relintPrimalbertini(DimMat,Prob,primalExpT)
% relintPrimalbertini(DimMat,Prob) find point in relative interior in Primal with bertini
%   INPUT: DimMat is order of symmetric matrices A_i and C,
%           Prob is sswf SDP problem
%   OUTPUT: Numerical point close to relative interior point in Feasible set

 % Cycling number parameters for Bertini
  % primalExpT =  2^(DimMat-0);  % Cycle Number [Hauenstein21]
  primalExpT;
  dualExpT = 1;
  primalDualExpT = 1;
  
  try
      [OutcomeP,Xnum] = PrimalFeasibility(Prob.A, Prob.b, Prob.c, primalExpT);

      msgID = 'mySDP:NoError';
      msgtext = 'Success';
      MEPrimal = MException(msgID,msgtext);
      
  catch e %e is an MException struct
      warning('Error in PrimalFeasibility ');
      fprintf(1,'The identifier was:\n%s\n',e.identifier);
      fprintf(1,'There was an error! The message was:\n%s\n',e.message);
      
      Xnum = eye(DimMat);
      OutcomeP = 'ERROR';
      MEPrimal = e;
  end
  
if issymmetric(Xnum) ~= 1
  warning('Forcing symmetric matrix')
  Xnum    = (Xnum+Xnum')/2;
end
