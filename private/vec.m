function [vector] = vec(Matrix)
% This function converts a matrix into a column vector stacking the columns
% vertically
    vector = reshape(Matrix,1,[])';
%     vector = matrix(:);

end

