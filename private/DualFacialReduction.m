% Proof-of-concept implementation of the SDP solver in 
% J. Hauenstein, A. Liddell, S. McPherson, and Y. Zhang.
% Numerical algebraic geometry and semidefinite programming.
% Copyright 2021

function [Aout,bout,Cout,ObjectiveValueConstant] = DualFacialReduction(S,y,A,b,C)
% INPUT: S & y is feasible for problem with A,b,C
% OUTPUT: Aout,bout,Cout is corresponding problem after facial reduction
%         ObjectiveValueConstant is constant part of objective function

    k = length(y);
    n = size(S,1);
    
    [~,E,V] = svd(S);
    r = sum(diag(E) > 1e-8);
    N = V(:,r+1:end);
    
    if size(N,2) == 0
        % no facial reduction needed
        Aout = A;
        bout = b;
        Cout = C;
        ObjectiveValueConstant = 0;
        return;
    end
    
    % setup linear system described by null space
    LHS = zeros(size(N,1)*size(N,2),k);
    for i = 1:size(N,2)
        for j = 1:k
            LHS((i-1)*size(N,1)+1:i*size(N,1),j) = reshape(A(j,:),[n n])*N(:,i);
        end
    end
    
    % compute implicit representation of solution set: y = W*yhat + z
    z = y;
    W = null(LHS);
    
    % size of new variables yhat
    r = size(W,2);
    
    % setup Aout
    Aout = zeros(r,n^2);
    for i = 1:r
        for j = 1:k
            Aout(i,:) = Aout(i,:) + W(j,i)*A(j,:);
        end
    end

	% setup bout and ObjectiveValueConstant
    bout = b*W;
    ObjectiveValueConstant = b*transpose(z);
    
    % setup Cout
    Cout = C - z*A;
end
