
function [Outcome,Sout,yout] = DualFeasibility(A,b,C,Texp)
% INPUT: A = k x n^2 matrix (k = # of linear equations, X is n x n matrix)
%        b = k vector
%        C = n^2 vector
% OUTPUT: Outcome = 0 if dual is strictly feasible, 1 if dual is
%                   feasible but not strictly feasible, 2 if dual is infeasible
%         Sout = n x n matrix when dual is feasible
%         yout = k vector when dual is feasible

k = length(b);
n = sqrt(length(C));
C = reshape(C,[n n]);

% compute scale of A & b
rescaleVals = zeros(1,k);
for j = 1:k
  rescaleVals(j) = max(abs(A(j,:)));
  rescaleVals(j) = max(rescaleVals(j),abs(b(j)));
end

% setup start point
hhat = 1;
yhat = zeros(k,1);
lhat = 0;
bhat = 1;
sigmahat = floor(min(eig(C))-1);
Shat = C-sigmahat*eye(n,n);
Xhat = inv(Shat);

Pt = zeros(1,1+n*(n+1)/2+1+n*(n+1)/2+k+1);
c = 1;
Pt(c) = hhat;
c = c+1;
for i = 1:n
    for j = i:n
        Pt(c) = Shat(i,j);
        c = c+1;
    end
end
for j = 1:k
    Pt(c) = yhat(j);
    c = c+1;
end
Pt(c) = lhat;
c = c+1;
for i = 1:n
    for j = i:n
        Pt(c) = Xhat(i,j);
        c = c+1;
    end
end
Pt(c) = bhat;
c = c+1;

% write the start point to a file
CreateStart(Pt, 1, length(Pt), 'start');

% create system
syms t h l B;
X = zeros(n,n)*t;
S = zeros(n,n)*t;
y = zeros(k,1)*t;
PrimalVars = zeros(n*(n+1)/2+1,1)*t;
DualVars = zeros(1+n*(n+1)/2+k+1,1)*t;
p = 1;
d = 1;
DualVars(d) = h;
d = d +1;

for i = 1:n
    for j = i:n
        eval(['syms x',num2str(i),num2str(j)]);
        PrimalVars(p) = eval(['x',num2str(i),num2str(j)]);
        X(i,j) = PrimalVars(p);
        X(j,i) = PrimalVars(p);
        p = p + 1;
        eval(['syms s',num2str(i),num2str(j)]);
        DualVars(d) = eval(['s',num2str(i),num2str(j)]);
        %s is the dual variable
        S(i,j) = DualVars(d);
        S(j,i) = DualVars(d);
        d = d + 1;   
    end
end
PrimalVars(p) = B;
p = p+1;
for i = 1:k
    eval(['syms y',num2str(i)]);
    DualVars(d) = eval(['y',num2str(i)]);
    y(i) = DualVars(d);
    d = d+1;
end
DualVars(d) = l;
d = d+1;

% setup equations
Ct = C-t*sigmahat*eye(n,n);
F = zeros(n*(n+1)/2+1+n*(n+1)/2+k+1,1)*t;
f = 1;
for i = 1:k
    F(f) = (trace(reshape(A(i,:),[n n])*X) -(t*trace(reshape(A(i,:),[n n])*Xhat)))/rescaleVals(i);
    f = f+1;
end
F(f) = trace(X) + B - 1 - t*trace(Xhat);
f = f+1;

for i = 1:n
    for j = i:n
        F(f) = h*Ct(i,j) - S(i,j);
        if i==j
            F(f)= F(f)-l;
        end
        for m = 1:k
            F(f) = F(f) - y(m)*A(m,(i-1)*n+j);
        end
        f = f+1;
    end
end
Mult = S*X + X*S - 2*h*t*eye(n,n);
for i = 1:n
    for j = i:n
        F(f) = Mult(i,j);
        f = f+1;
    end
end
F(f) = B*(h-l) - h*t;
f = f+1;

% Create Bertini file for system
T = fopen('input','w');
fprintf(T,'CONFIG\n SecurityLevel: 1;\n ODEPredictor: 2;\n MaxNorm: 1e300;\n AMPSafetyDigits1: -2;\n AMPSafetyDigits2: -2;\n UserHomotopy: 2;\n EndgameBdry: 0.9;\n FinalTol: 1e-10;\nEND;\nINPUT\n hom_variable_group ');
for j = 1:length(DualVars)
    fprintf(T,'%s',char(DualVars(j)));
    if j < length(DualVars)
        fprintf(T,',');
    else
        fprintf(T,';\n');
    end
end
fprintf(T,' variable_group ');
for j = 1:length(PrimalVars)
    fprintf(T,'%s',char(PrimalVars(j)));
    if j < length(PrimalVars)
        fprintf(T,',');
    else
        fprintf(T,';\n');
    end
end

fprintf(T,' pathvariable tt;\n parameter t;\n t = tt^%d;\n function ',Texp);
for j = 1:length(F)
    fprintf(T,'f%d',j);
    if j < length(F)
        fprintf(T,',');
    else
        fprintf(T,';\n');
    end
end
for j = 1:length(F)
    fprintf(T,' f%d = %s;\n',j,char(F(j))); % print integer (%d) and string (%s)
end
fprintf(T,'END;\n');
fclose(T);

% run Bertini
% ### Esta modificacion es para usar bertini en windows or Linux!!!!
% eval('!./BertiniSolverLinux/bertini input start > /dev/null');%LINUX
%eval('!bertini.bat > NUL');%Windows

if ispc
    %system(['call bertini.bat','-echo'])
    eval('!bertini.bat > NUL');%WINDOWS
elseif isunix
    eval('!./BertiniSolverLinux/bertini input start > /dev/null');%LINUX
end


% read in output
[~,P] = ReadInStart(length(Pt),'finite_solutions');
P = real(P);

hout = P(1);
sout = P(2:1+n*(n+1)/2);
yout = P(2+n*(n+1)/2:1+n*(n+1)/2+k);
lout = P(2+n*(n+1)/2+k);
Sout = zeros(n,n);

if abs(hout) < 1e-10
    % infeasible
    Outcome = 2;
else
  % dehomogenize
   lout = lout/hout;
   sout = sout/hout;
   yout = yout/hout;
   d = 1;
   for i = 1:n
       for j = i:n
           Sout(i,j)= sout(d);
           Sout(j,i)=sout(d);
           d = d+1;
       end
   end
   
   if abs(lout) < 1e-10
       % feasible but not strictly feasible
       Outcome = 1; 
   elseif lout > 0
       % strictly feasible
       Outcome = 0;
   else
       % infeasible
       Outcome = 2;
   end
end

return
