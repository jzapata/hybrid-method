function [eqn,vars,Qsub,Q,varXY]=FixVars(EQS,NeqsSDP,NoXs,varXY,numXY,tol)

% some useful vars

% NoXs = DimMat*(DimMat+1)/2;

varX = varXY(1:NoXs);
varY = varXY(NoXs+1:end);

numX = numXY(1:NoXs);
numY = numXY(NoXs+1:end);


% %% Find those vars X that can be fixed
% M = rref(jacobian(EQS(1:NeqsSDP),varX));
% [Pivot,IndcolsA] = max(logical(M),[],2);
% 
% fixable = setdiff(1:length(varX),IndcolsA(Pivot))
% 
% %% LD columns in Q(Y)
% Q = jacobian(EQS,varX);
% Qsub = double(subs(Q,varY,numY));
% Qsub(abs(Qsub)<tol)=0;
% Qred = rref(Qsub);
% %Qred( ~any(Qred,2), : ) = [];
% [~,LIcolsQ] = max(logical(Qred),[],2);
% LIcolsQ = LIcolsQ(any(Qred')); % Delete pivots for zero rows
% LDcolsQ = setdiff((1:size(Qsub,2)),LIcolsQ)
% 
% setdiff(1:length(varX),IndcolsA(Pivot));
% % LDcolsQ
% %% Vars2Fix
% eqn = [];
% vars = [];
% 
% for i=1:length(LDcolsQ)
%     eqn = [eqn;varX(fixable(i))-numX(fixable(i))];
%     vars = [vars;varX(fixable(i))];
% % if ismember(LDcolsQ(i),fixable)
% %     eqn = [eqn;varX(LDcolsQ(i))-numX(LDcolsQ(i))];
% % end
% end

%% LD columns in Q(Y)
Q = jacobian(EQS,varX);
Qsub = double(subs(Q,varY,numY));
% % %  [UU,SS,VV]=svd(Qsub)
% % % 
% % % vpa(svd(Qsub),4)
% % % vpa(rref(Qsub),4)
% Qsub(abs(Qsub)<tol)=0;


Qred = Nrref(Qsub,tol); % Replace by LIC, GEcompiv, rho-vol
%Qred( ~any(Qred,2), : ) = [];
[~,LIcolsQ] = max(logical(Qred),[],2);
LIcolsQ = LIcolsQ(any(Qred')); % Delete pivots for zero rows
LDcolsQ = setdiff((1:size(Qsub,2)),LIcolsQ);



[QQ,LIcolsQ] = GEcompiv(Qred);
% vpa(QQ)
LDcolsQ = setdiff((1:size(Qsub,2)),LIcolsQ);


eqn = [];
vars = [];
for i=1:length(LDcolsQ)
%     eqn = [eqn;varX(LDcolsQ(i))-round(numX(LDcolsQ(i)),3)];
    eqn = [eqn;varX(LDcolsQ(i))-numX(LDcolsQ(i))];
    vars = [vars;varX(LDcolsQ(i))];
% if ismember(LDcolsQ(i),fixable)
%     eqn = [eqn;varX(LDcolsQ(i))-numX(LDcolsQ(i))];
% end
end
