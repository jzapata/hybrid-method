% fid = fopen('./EQS/msolveRUROUTPUT');
% tline = fgetl(fid);
% while ischar(tline)
%     disp('____')
%     disp(tline)
%     % regexp(tline, '(?<=\()[^)]*(?=\))', 'match', 'once')

%     tline = fgetl(fid);
% end
% fclose(fid);

% content = fileread("./EQS/msolveRUROUTPUT")
% content = '[0, [0, 3, 11, [''X3_4'', ''X3_3'', ''X2_3''], [0, 0, 1], [1, [[3, [0, -1, 0, 1]], [2, [-1, 0, 3]], [[[2, [-1, 0, 1]], 1], [[2, [1, 0, 1]], 1]]]]]]';

% pattern = '\[([^[\]]*)\]';
% % pattern = '\[([^]]*)\]';
% % pattern = '\[(*)]\]';
% matches = regexp(content, pattern, 'tokens')
% extractedLists = cell(size(matches))
% for i = 1:numel(matches)
%     % Recursively call the function for nested content
%     extractedLists{i} = extractLists(matches{i}{1});
% end


% path to  text file
filePath = './EQS/msolveRUROUTPUT';

% Read the content of the file
fileID = fopen(filePath, 'r');
fileContent = fscanf(fileID, '%c');
fclose(fileID);

% Preprocess the content
fileContent = strrep(fileContent, '''', '"');
fileContent = strrep(fileContent, newline, '');
fileContent = strrep(fileContent, ':', '');

% Parse the string using jsondecode
parsedStructure = jsondecode(fileContent);

% % Extract individual components
% DimensionVar = parsedStructure{1};
% sublist = parsedStructure{2};

% nvars = sublist{1}{2};
% deg = sublist{1}{3};
% vars = sublist{1}{4}; vars = sym(vars)';
% form = sublist{1}{5};
% subsublist = sublist{1}{6};

% placeholder2 = subsublist{1};
% lw = subsublist{1}{2};
% lwp = subsublist{1}{3};
% param = subsublist{1}{4};
% Extract individual components
DimenVar = parsedStructure{1};
sublist = parsedStructure{2};

nvars = sublist{2};
deg = sublist{3};

vars = sublist{4};
vars = sym(vars);
assume(vars,'real');
vars = vars';

form = sublist{5}';
subsublist = sublist{6};

% placeholder = subsublist{1};
RatPar = subsublist{2};
w = sym(RatPar{1}{2}');

try dw = sym(RatPar{2}{2}');
catch dw = sym(RatPar{2}');dw = dw(2:end); end

Vs = RatPar{3};

% find max degree of v_i's 
degV = 0 ;
for k = 1:length(Vs)
    try degV = max(degV,Vs{k}{1}{1});
    catch degV = max(degV,Vs{k}{1}(1)); end
end
V  = zeros(0,degV+1);

% create a matrix with coefficients of v_i's
for k = 1:length(Vs)
    try
    -Vs{k}{1}{2}'/Vs{k}{2}
    V = [V;sym(-Vs{k}{1}{2}')/sym(Vs{k}{2})]
    catch V = [V;sym(-Vs{k}{1}(2)')/sym(Vs{k}{2})];
    end
end

A = sym('A') ;
mont = [] ;
for deg = 1:length(w)
    mont = [ mont , A^(deg-1) ]
end


polw = sum(w.*mont)
poldw = sum(dw.*mont(1,1:length(dw)))
polsV = V.*mont(1,1:size(V,2))


%% TEST
if size(polsV,1) == length(vars)-1
    xtra = sym(zeros(1,degV+1));
    xtra(2) = sym(A)*poldw;
    if size(polsV,2)>1
    polsV = [polsV;xtra];
    else
        polsV = [polsV,zeros(size(polsV,1),1)];
        polsV = [polsV;xtra];
    end

end


fid = fopen('EQS/mapleCHECKratpar.mpl', 'wt');

  %% matrix X 
  matX = [char(varXmatsubs(1,:))];
  for i = 2:size(varXmat,1)
      newStr = strcat(',', char(varXmatsubs(i,:)));
      matX = [matX , newStr ];
  end
  matX = strcat('X := Matrix([',matX,']):');
  fprintf(fid, '%s\n', matX);


str = strcat('vars := ',char(vars),':');
fprintf(fid,'%s\n',str);

str = strcat('w := ',char(polw),':');
fprintf(fid,'%s\n',str);
str = 'rootz := RootOf(w,A);'
fprintf(fid,'%s\n',str);


fprintf(fid,'%s\n','Pols:=[');             % Open [
    for i=1:size(polsV,1)-1
      %str = strcat(char(EQS(i)),'=0,');
      str = strcat('(',char(sum(polsV(i,:))),')/(',char(poldw),'),')
      %newStr = erase(str,"_");
      newStr = str;
      fprintf(fid, '%s\n', newStr);  
    end
  
    %str = strcat(char(EQS(i+1)),'=0'); %Save last equation without ,
    str = strcat('(',char(sum(polsV(size(polsV,1),:))),')/(',char(poldw),')'); %Save last equation without ,
    %   newStr = erase(str,"_");
    newStr = str;   
    fprintf(fid, '%s\n', newStr);
    fprintf(fid,'%s\n',']:');               % Close ]

    
    

    str = 'vals := {allvalues(eval(Pols, A = rootz))}:';
    fprintf(fid, '%s\n', str);

    str = 'ecs := map(x -> convert(vars = x, listofequations), vals):';
    fprintf(fid, '%s\n', str);

    fprintf(fid,'%s\n','Nelem := [seq(k, k = 1 .. numelems(vals))]:');
    % fprintf(fid, '%s\n', "Xs := map(x -> subs(X, vars, x), evaluated_rootz);")
    fprintf(fid, '%s\n', "Xs := map(x -> eval(X, ecs[x][1]), Nelem):")

    % with(LinearAlgebra);
    str = 'with(LinearAlgebra):';
    fprintf(fid, '%s\n', str);
    % map(x -> Eigenvalues(x), Xs);

    
    str = "IsPSD := map(x -> IsDefinite(x, query = 'positive_semidefinite'), Xs);";
    fprintf(fid, '%s\n', str);

    % Use the following lines to compute eigenvalues explicitely 
    if false
        str = 'Eigs := map(x -> Eigenvalues(x), Xs):';
        fprintf(fid, '%s\n', str);
        
        str = 'BoolEigs := map(x -> map(y -> is(Im(y) = 0) and is(0 <= Re(evalf(y))), x), Eigs);';
        fprintf(fid, '%s\n', str);
        
        %% numerical values for eigenvalues
        str = 'evalf(Eigs);';
        fprintf(fid, '%s\n', str);   
    end

    fclose(fid); 

