function [Xber,OutcomeP,MEPrimal] = bertiniNumSol(DimMat,Prob, tol)

if true
    % Search interior point with Bertini
    fprintf("Running bertini...")

    primalExpT = 2^(DimMat-2);
    [Xber,OutcomeP,MEPrimal]  = relintPrimalbertini(DimMat, Prob, primalExpT)
    
    
    if rank(Xber) ~= size(Xber,1)
        warning('XBER is singular')
    else       
        warning('XBER HAS FULL RANK!! Using different Cycle Number...')
        repber=0;
        while (norm(Prob.A*vec(Xber)-Prob.b')>tol | OutcomeP==2) & repber<DimMat
            fprintf('CyNumber: 2^(%d)',DimMat-repber); % Cycle Number [Hauenstein21]
            [Xber,OutcomeP,MEPrimal]  = relintPrimalbertini(DimMat, Prob, 2^(repber+2))
            if norm(Prob.A*vec(Xber)-Prob.b')<tol
                OutcomeP = 1
            end
            repber = repber+1
        end
    end
end