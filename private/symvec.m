function [vector] = symvec(symMat)
% Converts symmetric matrix into a column deleting the lower triangular
% part keeping the diagonal and stacking the remaning sub-columns from left
% to right as a single column vector.
vector = [];

for i=1:size(symMat,1)
    for j=1:size(symMat,2)
        if j>=i
            vector = [vector;symMat(i,j)];
        end
    end
end
end
