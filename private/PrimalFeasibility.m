% Proof-of-concept implementation of the SDP solver in 
% J. Hauenstein, A. Liddell, S. McPherson, and Y. Zhang.
% Numerical algebraic geometry and semidefinite programming.
% Copyright 2021

function [Outcome,Xout] = PrimalFeasibility(A,b,C,Texp)
% INPUT: A = k x n^2 matrix (k = # of linear equations, X is n x n matrix)
%        b = k vector
%        C = n^2 vector
% OUTPUT: Outcome = 0 if primal is strictly feasible, 1 if primal is feasible 
%                   but not strictly feasible, 2 if primal is infeasible
%         Xout = n x n matrix when primal is feasible

k = length(b);
n = sqrt(length(C));

% compute scale of A & b
rescaleVals = zeros(1,k);
for j = 1:k
  rescaleVals(j) = max(abs(A(j,:)));
  rescaleVals(j) = max(rescaleVals(j),abs(b(j)));
end

% setup start point
hhat = 1;
Xhat = eye(n,n);
lhat = 0;
Shat = eye(n,n);
yhat = zeros(k,1);
ghat = 1;
Pt = zeros(1,1+n*(n+1)/2+1+n*(n+1)/2+k+1);
c = 1;
Pt(c) = hhat;
c = c+1;
for i = 1:n
    for j = i:n
        Pt(c) = Xhat(i,j);
        c = c+1;
    end
end
Pt(c) = lhat;
c = c+1;
for i = 1:n
    for j = i:n
        Pt(c) = Shat(i,j);
        c = c+1;
    end
end
for j = 1:k
    Pt(c) = yhat(j);
    c = c+1;
end
Pt(c) = ghat;

% write the start point to a file
CreateStart(Pt, 1, length(Pt), 'start');

% create system
syms t h l g;
X = zeros(n,n)*t;
S = zeros(n,n)*t;
y = zeros(k,1)*t;
PrimalVars = zeros(1+n*(n+1)/2+1,1)*t;
DualVars = zeros(n*(n+1)/2+k+1,1)*t;
p = 1;
PrimalVars(p) = h;
p = p+1;
d = 1;
for i = 1:n
    for j = i:n
        eval(['syms x',num2str(i),num2str(j)]);
        PrimalVars(p) = eval(['x',num2str(i),num2str(j)]);
        X(i,j) = PrimalVars(p);
        X(j,i) = PrimalVars(p);
        p = p + 1;
        eval(['syms s',num2str(i),num2str(j)]);
        DualVars(d) = eval(['s',num2str(i),num2str(j)]);
        S(i,j) = DualVars(d);
        S(j,i) = DualVars(d);
        d = d + 1;   
    end
end
PrimalVars(p) = l;
p = p+1;
for i = 1:k
    eval(['syms y',num2str(i)]);
    DualVars(d) = eval(['y',num2str(i)]);
    y(i) = DualVars(d);
    d = d+1;
end
DualVars(d) = g;
d = d+1;

% setup equations
C = t*eye(n,n);
F = zeros(n*(n+1)/2+1+n*(n+1)/2+k+1,1)*t;
f = 1;
for i = 1:k
    F(f) = (trace(reshape(A(i,:),[n n])*X) - h*((1-t)*b(i) + t*trace(reshape(A(i,:),[n n])*Xhat)))/rescaleVals(i);
    f = f+1;
end
for i = 1:n
    for j = i:n
        F(f) = C(i,j) - S(i,j);
        for m = 1:k
            F(f) = F(f) - y(m)*A(m,(i-1)*n+j);
        end
        f = f+1;
    end
end
F(f) = trace(S) + g - 1 - t*trace(Shat);
f = f+1;
Mult = S*(X + l*eye(n,n)) + (X + l*eye(n,n))*S - 2*h*t*eye(n,n);
for i = 1:n
    for j = i:n
        F(f) = Mult(i,j);
        f = f+1;
    end
end
F(f) = g*(l + h) - h*t;
f = f+1;


% Create Bertini file for system
T = fopen('input','w');
fprintf(T,'CONFIG\n SecurityLevel: 1;\n ODEPredictor: 2;\n MaxNorm: 1e300;\n AMPSafetyDigits1: -2;\n AMPSafetyDigits2: -2;\n UserHomotopy: 2;\n EndgameBdry: 0.9;\n FinalTol: 1e-10;\n EndgameNum: 2;\n CycleTimeCutoff: 0.85;\n RatioTimeCutoff: 0.8;\n NumSamplePoints: 9;\nEND;\nINPUT\n hom_variable_group ');
for j = 1:length(PrimalVars)
    fprintf(T,'%s',char(PrimalVars(j)));
    if j < length(PrimalVars)
        fprintf(T,',');
    else
        fprintf(T,';\n');
    end
end
fprintf(T,' variable_group ');
for j = 1:length(DualVars)
    fprintf(T,'%s',char(DualVars(j)));
    if j < length(DualVars)
        fprintf(T,',');
    else
        fprintf(T,';\n');
    end
end
fprintf(T,' pathvariable tt;\n parameter t;\n t = tt^%d;\n function ',Texp);
for j = 1:length(F)
    fprintf(T,'f%d',j);
    if j < length(F)
        fprintf(T,',');
    else
        fprintf(T,';\n');
    end
end
for j = 1:length(F)
    fprintf(T,' f%d = %s;\n',j,char(F(j))); % print integer (%d) and string (%s)
end
fprintf(T,'END;\n');
fclose(T);

% run Bertini
% ### Esta modificacion es para usar bertini en windows o LINUX!!!!
% eval('!./BertiniSolverLinux/bertini input start > /dev/null');%LINUX
% eval('!bertini.bat > NUL');%WINDOWS

if ispc
    %system(['call bertini.bat','-echo'])
    eval('!bertini.bat > NUL');%WINDOWS
elseif isunix
    eval('!./BertiniSolverLinux/bertini input start > /dev/null');%LINUX
end


% read in output
[~,P] = ReadInStart(length(Pt),'finite_solutions');
P = real(P);

hout = P(1);
xout = P(2:1+n*(n+1)/2);
lout = P(2+n*(n+1)/2);

Xout = zeros(n,n);

% determine results
if abs(hout) < 1e-10
    % infeasible
    Outcome = 2;
    Xout = zeros(n,n);  
else
   % dehomogenize
   lout = lout/hout;
   xout = xout/hout;
   d = 1;
   for i = 1:n
       for j = i:n
           Xout(i,j)= xout(d);
           Xout(j,i) = xout(d);
           d = d+1;
       end
   end
   
   if abs(lout) < 1e-10
       % feasible but not strictly feasible
       Outcome = 1; 
   elseif lout > 0
       % infeasible
       Outcome = 2;
   else
       % strictly feasible
       Outcome = 0;
   end
end

return
