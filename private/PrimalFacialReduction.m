% Proof-of-concept implementation of the SDP solver in 
% J. Hauenstein, A. Liddell, S. McPherson, and Y. Zhang.
% Numerical algebraic geometry and semidefinite programming.
% Copyright 2021

function [Aout,bout,Cout] = PrimalFacialReduction(X,A,b,C)
% INPUT: X is feasible for problem with A,b,C
% OUTPUT: Aout,bout,Cout is corresponding problem after facial reduction

    % compute null space
    [~,E,V] = svd(X);
    r = sum(diag(E) > 1e-10);
    N = V(:,r+1:end);
    
    A = full(A);
    b = full(b);
    
    % compute corresponding equations from null vectors
    for i = 1:size(N,2)
       Av = ConvertToMatrix(N(:,i));
       bv = zeros(1,size(Av,1));
       A = [A;Av];
       b = [b bv];
    end
    
    % find linearly independent equations
    [Aout,bout] = LinearIndependence(A,b);
    Cout = C;
return;


% Here we are adding a set of symmetric matrices corresponding to the 
% eigenvectors of the null space of the inner point X

function [A] = ConvertToMatrix(v)
    n = length(v);
    A = zeros(n,n^2);    
    for j = 1:n
        a = zeros(n,n);
        a(:,j) = v/2;
        a = a + a';
        A(j,:) = reshape(a,[1 n^2]);
    end
return;

function [Aout,bout]= LinearIndependence(A,b)
    [U,E,~]= svd(A);
    r = sum(diag(E) > 1e-8);
    M = pinv(E)*transpose(U);
    Aout = M*A;
    Aout = Aout(1:r,:);
    bout = M*transpose(b);
    bout = transpose(bout(1:r));
return;
