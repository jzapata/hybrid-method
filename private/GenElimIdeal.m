
function [newEQS] = GenElimIdeal(EQS , varX)
  fid = fopen('EQS/GenElimIdeal.mpl', 'wt');


  %% Polynomial System
  % SAVE EQS
  fprintf(fid,'%s\n','F:=[');             % Open [
  for i=1:size(EQS,1)-1
    %str = strcat(char(EQS(i)),'=0,');
    str = strcat(char(EQS(i)),',');
    %newStr = erase(str,"_");
    newStr = str;
    fprintf(fid, '%s\n', newStr);  
  end

  %str = strcat(char(EQS(i+1)),'=0'); %Save last equation without ,
  str = char(EQS(i+1)); %Save last equation without ,
  %   newStr = erase(str,"_");
  newStr = str;   
  fprintf(fid, '%s\n', newStr);
  fprintf(fid,'%s\n',']:');               % Close ]


  %% Variables
  str = char([varX]);
  
  newStr = str;
  newStr = erase(newStr,"matrix");
  newStr = erase(newStr,"[");
  newStr = erase(newStr,"]");
  newStr = erase(newStr,"(");
  newStr = erase(newStr,")");
  %   newStr = strcat('},[',newStr,']):'); % {eqs}  <-- set
  newStr = strcat('V:={',newStr,'}:');% [eqs]   <-- list
  fprintf(fid, '%s\n', newStr);




  %% ELmination Ideal
  % with(PolynomialIdeals);
  % F := Generators(EliminationIdeal(PolynomialIdeal(F),{X1_1, X1_3, X2_3, X3_3, Y3_1}));
  fprintf(fid,'%s\n','with(PolynomialIdeals):');
%   fprintf(fid,'%s\n','F := Generators(EliminationIdeal(PolynomialIdeal(F),V));');  
     
    fprintf(fid,'%s\n',' try F:=timelimit(60,Generators(EliminationIdeal(PolynomialIdeal(F),V)));');  
    
    fprintf(fid, '%s\n', "catch: F:={1};");

    fprintf(fid, '%s\n', 'end try:');


  fprintf(fid, '%s\n', 'save F, "EQS/GenElimIdeal.txt";');
  fclose(fid);

  if ispc
    %system(['call bertini.bat','-echo'])
    %eval('!bertini.bat > NUL');%WINDOWS
    eval('!maple.bat');%WINDOWS
    elseif isunix
        %eval('!./BertiniSolverLinux/bertini input start > /dev/null');%LINUX
    %     fprintf('ToDo');
        % eval('!msolve -f EQS/EQSmsolve.txt -o EQS/msolveOUTPUT')
        eval('!maple EQS/GenElimIdeal.mpl >null')
        % eval('!maple EQS/GenElimIdeal.txt >null')
        % disp('finished!!')
    %     break
    end


    msolveEQS = fileread("./EQS/GenElimIdeal.txt");


    %% Format for msolve
    if length(varX) == 1
        msolvevars = char(varX);
    else
        msolvevars = extractBetween(char([varX]),'[',']');
    end

    msolveEQS=erase(msolveEQS,char(10));
    msolveEQS=erase(msolveEQS,'\');
    msolveEQS=erase(msolveEQS,' ');
    msolveEQS=extractBetween(msolveEQS,'{','}');
    msolveEQS=split(msolveEQS,',');

    fid = fopen('EQS/EQSmsolve.txt', 'wt');
    fprintf(fid, '%s\n', char(msolvevars));           %% Save vars

    fprintf(fid, '0\n');                    %% Characteristic of the field
    
                        
  for i=1:size(msolveEQS,1)-1                     %% SAVE EQS
    str = strcat(char(msolveEQS(i)),',');
    %     newStr = erase(str,"_");
    newStr = str;
    fprintf(fid, '%s\n', newStr);  
  end

  str = strcat(char(msolveEQS(end))); %Save last equation without ,
%   newStr = erase(str,"_");
  newStr = str;
  fprintf(fid, '%s\n', newStr);

  fclose(fid);



    newEQS = [];

    

end