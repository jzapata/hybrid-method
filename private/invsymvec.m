
function [Mat] = invsymvec(vector)
% Converts vector into symmetric matrix (inverse of symvec function)

dim = (-1+sqrt(1+8*length(vector)))/(2);
Mat = zeros(dim);

while ~isempty(vector)
    for i=1:dim
        for j=i:dim
%             fprintf('%d-%d\n',i,j);
            Mat(i,j) = vector(1);
            vector(1)=[];
        end
    end
%     Mat
end
D = diag(Mat);
Mat = (Mat+Mat'-diag(D));

end