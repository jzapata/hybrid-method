function [IdRowsU, IdRowsV, numY, numXY] = iotaCols(DimMat, numXmat)

[~,LIC] = GEcompiv(numXmat);
LIC=sort(LIC);
iota=setdiff(1:DimMat,LIC);
numY=-inv(numXmat(LIC,LIC))*numXmat(iota,LIC)';

IdRowsU = LIC;
IdRowsV = iota;
numXY =[];
for i=1:DimMat
  numXY = [numXY , numXmat(i,i:end)];
end
d = DimMat - length(IdRowsV);
for i=1:size(numY,1)
    numXY = [numXY , numY(i,:)];
end