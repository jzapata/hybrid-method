function [SolIntervls,msolvEigs,Solx,Soly,SolL,msolveNote]=CheckMsolveOutputs(varX,varY,varL,varXmatsubs)




%Number of vars of each type
Nx = length(varX);      %% #X vars
Ny = length(varY);      %% #Y vars
Nl = length(varL);      %% #LangrangeMultipliers


%Lists to save respective solutions
Solx = [];
Soly = [];
SolL = [];

SolIntervls = [];
msolvEigs = [];
msolveNote = {};


File2Load = sprintf('./EQS/msolveOUTPUT');
msolOUTPUT = fileread(File2Load);


if msolOUTPUT(2) == '1'
    disp('<strong>msolve: Sol set is pos dim in complex numbers</strong>')
    msolveNote = {'Pos dim syst'};
elseif msolOUTPUT(2) == '-'
    disp('<strong>msolve: Sol set is empty</strong>')
    msolveNote = {'empty syst'};

elseif msolOUTPUT(2) == '0'
    % msolOUTPUT%%
    str = strrep(msolOUTPUT,']]]',']]');
    str = strrep(str,'[[[','[[');
    str = extractBetween(str,'[[',']]');
    str = strcat('[',str,']');
    str = strrep(str,'], [','];[');
    % str%%
    SolIntervls = [];
    
    for i = 1:size(str,1)
        SolIntervls(i).Mat = str2num(cell2mat(str(i)));
%         unfoldStruct(Sols)%TEST
        vecSol = SolIntervls(i).Mat(:,1);%vector with solutions for X,Y,L
        
        Solx = vecSol(1:Nx);
        Soly = vecSol(Nx+1:end-Nl);
        if ~Nl
            SolL = vecSol(end-Nl+1:end);
        end

%         MAT = invsymvec(vecSol(1:Nx));%Matrix X
        MAT = subs(varXmatsubs,varX,vecSol(1:Nx)');
        eigMAT = eigs(double(MAT));
        %eigMAT = double(eig(MAT))
        %eigMAT(abs(eigMAT)<10e-6) = 0;
        msolvEigs = [msolvEigs;eigMAT'];

        if all(eigMAT>=0)
            disp('PSD Solution was found:')
            MAT
        end
%         if eigMAT>=0
%             disp('POS EIGENVALUES!!!');
%             msolveNote = {'Success'};
%             return;
%         end
%         msolveNote = {'Neg EigVals'};
    end
end
end