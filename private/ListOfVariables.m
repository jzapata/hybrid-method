function [XYvar,Xvar,UYvar,VYvar] = ListOfVariables(DimMat,IdRowsU,IdRowsV)
% List of variables in Incidence Variety system
% INPUT:  DimMat is order of symmetric matrices matrices A_i and C, IdRowsV
% is the set of rows to be fixed
% OUTPUT: Matrix Ured with same range(X), rows corresponding to identity
% matrix in Y and numerical solution for Y

n = DimMat;
d = DimMat - length(IdRowsV);


%% X variables %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Xvar=sym('X', [n n]);
assume(Xvar,'real');
for j=2:n
    for i=1:j-1
        Xvar(j,i)=Xvar(i,j);
    end
end


%% VY matrix %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
VYvar = sym('Y',[n n-d]);
assume(VYvar,'real');
iota = IdRowsV;
try
  % Identity SubMatrix 
  VYvar(iota,:)=eye(n-d);
catch 
  warning('X has full rank!!!')
end


%% UY matrix %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
UYvar = sym('Y',[n d]);

UYvar(IdRowsU,:)=eye(d);% Identity SubMatrix
UYvar(IdRowsV,:)=-1*VYvar(IdRowsU,:)';


%% List of variables XY %%%%%%%%%%%%%%%%%%%%%%%%%
XYvar =[];
for i=1:n
  XYvar = [XYvar , Xvar(i,i:end)];
end
for i=1:n
  if ~ismember(i,iota)
    XYvar = [XYvar , VYvar(i,:)];
  end
end
assume(XYvar,'real');
assume(Xvar,'real');
assume(VYvar,'real');
assume(UYvar,'real');




