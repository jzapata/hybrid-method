% Proof-of-concept implementation of the SDP solver in 
% J. Hauenstein, A. Liddell, S. McPherson, and Y. Zhang.
% Numerical algebraic geometry and semidefinite programming.
% Copyright 2021

function [Outcome,Xout,Sout,yout,PO,DO] = HomPrimalDualSolver(A,b,C,Texp)
% INPUT: A = k x n^2 matrix (k = # of linear equations, X is n x n matrix)
%        b = k vector
%        C = n^2 vector
% OUTPUT: Outcome = 5 if both are primal and dual optimal values attained
%                   6 = primal optimal value is attained, dual is not
%                   7 = dual optimal value is attained, primal is not
%                   8 = primal and dual optimal values are not attained
%         Xout = primal solution
%         Sout,yout = dual solution
%         PO = primal optimal value
%         DO = dual optimal value
 
k = length(b);
n = sqrt(length(C));

% setup start point
yhat = zeros(k,1);
Q = reshape(C,[n n]);
for i=1:k
    Q = Q-yhat(i)*full(reshape(A(i,:),[n,n]));
end
sigma = floor(min(eig(Q))-1);
Shat = Q-sigma*eye(n,n);
Xhat = inv(Shat);
Pt = zeros(1,2+n*(n+1)/2+n*(n+1)/2+k);
c = 1;
Pt(c) = 1;
c = c+1;
for i = 1:n
    for j = i:n
        Pt(c) = Xhat(i,j);
        c = c+1;
    end
end
Pt(c) = 1;
c = c+1;
for i = 1:n
    for j = i:n
        Pt(c) = Shat(i,j);
        c = c+1;
    end
end
for j = 1:k
    Pt(c) = yhat(j);
    c = c+1;
end

% write the start point to a file
CreateStart(Pt, 1, length(Pt), 'start');

% create system
syms t hp hd;
X = zeros(n,n)*t;
S = zeros(n,n)*t;
y = zeros(k,1)*t;
PrimalVars = zeros(1+n*(n+1)/2,1)*t;
DualVars = zeros(1+n*(n+1)/2+k,1)*t;

p = 1;
PrimalVars(p) = hp;
p = p+1;
d = 1;
DualVars(d) = hd;
d = d+1;
for i = 1:n
    for j = i:n
        eval(['syms x',num2str(i),num2str(j)]);
        PrimalVars(p) = eval(['x',num2str(i),num2str(j)]);
        X(i,j) = PrimalVars(p);
        X(j,i) = PrimalVars(p);
        p = p + 1;
        eval(['syms s',num2str(i),num2str(j)]);
        DualVars(d) = eval(['s',num2str(i),num2str(j)]);
        S(i,j) = DualVars(d);
        S(j,i) = DualVars(d);
        d = d + 1;   
    end
end
for i = 1:k
    eval(['syms y',num2str(i)]);
    DualVars(d) = eval(['y',num2str(i)]);
    y(i) = DualVars(d);
    d = d + 1;
end

% setup equations
Ct = hd*(reshape(C,[n n]) - t*sigma*eye(n,n));
F = zeros(n*(n+1)/2+n*(n+1)/2+k,1)*t;
f = 1;
for i = 1:k
    F(f) = trace(reshape(A(i,:),[n n])*X) - hp*((1-t)*b(i) + t*trace(reshape(A(i,:),[n n])*Xhat));
    f = f+1;
end
for i = 1:n
    for j = i:n
        F(f) = Ct(i,j) - S(i,j);
        for m = 1:k
            F(f) = F(f) - y(m)*A(m,(i-1)*n+j);
        end
        f = f+1;
    end
end
Mult = S*X + X*S - 2*hd*hp*(t*eye(n,n));
for i = 1:n
    for j = i:n
        F(f) = Mult(i,j);
        f = f+1;
    end
end

% Create Bertini file for system
T = fopen('input','w');
fprintf(T,'CONFIG\n SecurityLevel: 1;\n ODEPredictor: 2;\n MaxNorm: 1e300;\n AMPSafetyDigits1: -2;\n AMPSafetyDigits2: -2;\n UserHomotopy: 2;\n EndgameBdry: 0.9;\n FinalTol: 1e-10;\n EndgameNum: 2;\nEND;\nINPUT\n hom_variable_group ');
for j = 1:length(PrimalVars)
    fprintf(T,'%s',char(PrimalVars(j)));
    if j < length(PrimalVars)
        fprintf(T,',');
    else
        fprintf(T,';\n');
    end
end
fprintf(T,' hom_variable_group ');
for j = 1:length(DualVars)
    fprintf(T,'%s',char(DualVars(j)));
    if j < length(DualVars)
        fprintf(T,',');
    else
        fprintf(T,';\n');
    end
end
fprintf(T,' pathvariable tt;\n parameter t;\n t = tt^%d;\n function ',Texp);
for j = 1:length(F)
    fprintf(T,'f%d',j);
    if j < length(F)
        fprintf(T,',');
    else
        fprintf(T,';\n');
    end
end
for j = 1:length(F)
    fprintf(T,' f%d = %s;\n',j,char(vpa(F(j),15))); % print integer (%d) and string (%s)
end
fprintf(T,'END;\n');
fclose(T);

% run Bertini
% ### Vamo a comentar!!!!
%eval('!bertini input start > /dev/null');
eval('!bertini.bat > NUL');

% read in output
[~,P] = ReadInStart(length(Pt),'real_solutions');
P = real(P);
hpout = P(1);
xout = P(2:1+n*(n+1)/2); 
hdout = P(2+n*(n+1)/2);
sout = P(3+n*(n+1)/2:2+n*(n+1)/2+n*(n+1)/2);
yout = P(n*(n+1)+3:end);
Xout = zeros(n,n);
Sout = zeros(n,n);
PO = 0;
DO = 0;

if abs(hpout) < 1e-10 && abs(hdout) < 1e-10 
    % both at infinity
    Outcome = 8;  
elseif abs(hdout) < 1e-10 
    % dehomogenize dual
    Outcome = 7;
    sout = sout/hdout;
    yout = yout/hdout;
    d = 1;
    for i=1:n
        for j=i:n
            Sout(i,j) = sout(d);
            Sout(j,i) = sout(d);
            d = d+1;
        end
    end
    DO = yout*transpose(b);
elseif abs(hpout) < 1e-10
    % dehomogenize primal
    Outcome = 6;
    xout = xout/hpout;
    d = 1;
    for i = 1:n
        for j = i:n
           Xout(i,j)= xout(d);
           Xout(j,i)= xout(d);
           d = d +1;
        end
     end
 
     PO = trace(reshape(C,[n n])*Xout);
else
    % both are attained
    Outcome = 5;
    sout = sout/hdout;
    yout = yout/hdout;
    d = 1;
    for i = 1:n
        for j = i:n
            Sout(i,j) = sout(d);
            Sout(j,i) = sout(d);
            d = d+1;
        end
    end
    DO = yout*transpose(b);
    
    xout = xout/hpout; 
    d=1;
    for i = 1:n
        for j = i:n
            Xout(i,j)= xout(d);
            Xout(j,i)= xout(d);
            d = d +1;
        end
    end
    PO = trace(reshape(C,[n n])*Xout);
end

return
