function [F,IndexParVars,Xvarmat] = ListOfEquations(DimMat,Prob,Xvarmat,Yvarmat, IdRowsV, IdRowsU)
% ListOfEquations(DimMat,Prob,Xvar,Yvar) gives the list of equations in the
% "Incidence Variety system" after using SDP constraints and elimnating
% redundancies [see: Exact algorithms for LMI - Naldi et al.]
%   INPUT:  DimMat is order of symmetric matrices matrices A_i and C, Prob is
%           the SDP problem
%   OUTPUT: List of equations F

Xvarori = Xvarmat;%%TEST
Ax_b = Prob.A*vec(Xvarmat)-Prob.b'; % SDP equality constraints

Aaux =  [];
for i=1:size(Prob.A,1)
Aaux = [Aaux;symvec(reshape(Prob.A(i,:),DimMat,DimMat))'];
end
%
Ared = rref(Aaux);
Ared = Ared(any(Ared,2),:);
[~,IdcolsA] = max(logical(Ared),[],2);
IdcolsA = IdcolsA(any(Ared'));%Check zero rows(not happenning if Ai's are LI)

Xvarvec = symvec(Xvarmat);

%Isolate some variables in linear equations
eqn = Ax_b == 0;
vars = Xvarvec(IdcolsA);
S = solve(eqn,vars);

% F = vertcat(vec(Xvar*Yvar), Ax_b)'; %%% with Ax_b with Red
% F(F==0)=[];

%% TEST LinChangVars
% M = magic(DimMat);
% % M = 2*eye(DimMat);
% Xvarmat = (M'*Xvarmat*M)


% % % % Solve linear equations for some x's
eqns = vars;
if ~isempty(fieldnames(S))
    eqns = fieldnames(S);
    for i = 1:numel(eqns)
      Xvarmat = subs(Xvarmat,vars(i),S.(eqns{i}));
    end
else
    Xvarmat = subs(Xvarmat,vars,S);
end 

% Xvar
% Yvar


XYeq = Xvarmat*Yvarmat;
XYeq = XYeq([IdRowsV,IdRowsU],:);

%Removing Redundancies
for ii=1:size(XYeq,1)
for jj=1:size(XYeq,2)
  if ii<jj
  XYeq(ii,jj)=0;
  end
end
end


%%TEST
IndexParVars = setdiff([1:length(Xvarvec)],IdcolsA);
F = vertcat(vec(XYeq));   %%% without Ax_b without Red
F(F==0)=[];


% IndexParVars = [1:length(Xvarvec)]
% XYeq = Xvarori*Yvarmat;
% %Removing Redundancies
% for ii=1:size(XYeq,1)
% for jj=1:size(XYeq,2)
%   if ii<jj
%   XYeq(ii,jj)=0;
%   end
% end
% end
% % F = vertcat(vec(XYeq) ,  Ax_b ) %%% With Ax_b without Red
% F(F==0)=[];

% % % vpa(F,3)




