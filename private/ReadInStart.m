% Proof-of-concept implementation of the SDP solver in 
% J. Hauenstein, A. Liddell, S. McPherson, and Y. Zhang.
% Numerical algebraic geometry and semidefinite programming.
% Copyright 2021

function [numPts, Pts] = ReadInStart(numVars, fileLocation)

% open file
RF = fopen(fileLocation, 'r');

% make sure the file exists
if RF == -1
    % file does not exist
    disp(['The file ''', fileLocation, ''' does not exist!']);
    numPts = 0;
    Pts = 0;
    return;
end;

% read in the number of points
numPts = fscanf(RF, '%d', 1);

% setup Pts
Pts = zeros(numPts, numVars);

% loop through to read in the points
tempR = 0; tempI = 0;
for k = 1:numPts
    % read in the point
    for j = 1:numVars
        tempR = fscanf(RF, '%e', 1);
        tempI = fscanf(RF, '%e', 1);
        Pts(k,j) = tempR + 1i * tempI;
    end;
end;

% close file
fclose(RF);

return;

