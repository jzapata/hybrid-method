F:=[
X2_3*Y3_1 - 2*X3_4 - Y1_1*(X3_3/2 + 1/2) - 1,
X3_4*Y3_1 - X2_3*Y1_1,
Y1_1 - X3_3/2 - 1/2,
X2_3 + X3_3*Y3_1,
X3_4*Y3_2 - X2_3*Y1_2 + 1,
Y1_2 - X2_3,
X3_4 + X3_3*Y3_2,
7*L1 - 30*L2 + 88*L3 + 76*L4 + 10*L5 + 25*L6 + 17*L7 - 1,
L4*Y3_1 - (L1*Y1_1)/2 - L3/2 + L7*Y3_2,
L7 - 2*L1 + L2*Y3_1 + L5*Y3_2,
L3 - L2*X2_3 - L1*(X3_3/2 + 1/2),
L6 - L5*X2_3,
L1*X2_3 + L2*X3_4 + L4*X3_3,
L5*X3_4 + L7*X3_3
]:
V:={X2_3, X3_3, X3_4}:
with(PolynomialIdeals):
 try F:=timelimit(60,Generators(EliminationIdeal(PolynomialIdeal(F),V)));
catch: F:={1};
end try:
save F, "EQS/GenElimIdeal.txt";
