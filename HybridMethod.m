%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Proof-of-Concept Implementation of "Hybrid" (Generating SDP)
%
% Description:
% This script provides a proof-of-concept implementation of the "Hybrid" 
% method for solving semidefinite programms (SDP) exactly. It 
% demonstrates the basic algorithm and key steps involved in the process.
%
% Author: J. Zapata
% Date:   2023-08-01
%
% Usage:
% To run this script, ensure that all dependencies are installed and that 
% the input data is correctly formatted. Modify parameters within the 
% script as needed to test different scenarios.
%
% Notes:
% - This is a prototype version intended for testing and demonstration 
%   purposes only. Optimization and error handling are minimal.
% - Future improvements could include error checking, performance 
%   enhancements, and additional functionalities.
%
% Dependencies:
% - Requires MATLAB R2020b or later.
% - bertini library
%
% Revision History:
% - 2023-08-20: Initial version created.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Load SDPs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

SDPNamePrimal = ["Permenter2018-4.3.2"];
SDP2Load = [strcat(SDPNamePrimal,'DStd.mat')];
SDPName = SDP2Load(1);

% Line =['<strong>%%%%%%%%%%%%%%%%%%%%%%%% HYBRID LOADING '...
%         '%%%%%%%%%%%%%%%%%%%%%%%%</strong>'];
% Line = strjust(pad(Line),'center');
% fprintf(2,'%s\n',Line)





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% PARAMETERS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tol = 1e-8;             % tolerance for eigenvalues
Xbertini = 1;
withCPM = 1;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Load problem
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



% SDP = [SDP;{SDPName}];
% File2Load = sprintf('./ExamplesMatlabFiles/ExamplesSedumiFormat/%s.mat',SDPName);
File2Load = sprintf('./Clean/%s',SDPName);

Prob = load(File2Load);
DimMat = Prob.K.s;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Numerical solution search
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[numXmat,OutcomeP,MEPrimal] = bertiniNumSol(DimMat,Prob, tol);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Matrix Ured and solution numXY for Incidence Variety system
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[IdRowsU, IdRowsV, numY, numXY] = iotaCols(DimMat, numXmat);

[varXY,varXmat,varUYmat,varVYmat] = ListOfVariables(DimMat,IdRowsU,IdRowsV);

numVYmat = subs(varVYmat,varXY,numXY);



%% Some useful vectors

NoXs = DimMat*(DimMat+1)/2;
varX = varXY(1:NoXs);
varY = varXY(NoXs+1:end);
numX = numXY(1:NoXs);
numY = numXY(NoXs+1:end);
allX = varX;

allX = varX;
allY = varY;
allXY = varXY;
allnumXY = numXY;
allnumX = numX;
allnumY = numY;


[F,ParVars,varXmatsubs] = ListOfEquations( DimMat , Prob, varXmat, varVYmat, IdRowsV,IdRowsU );


varXY = [varX(ParVars), varY];
numXY = [numX(ParVars), numY];
NoXs = length(ParVars);
varX = varXY(1:NoXs);
varY = varXY(NoXs+1:end);
numX = numXY(1:NoXs);
numY = numXY(NoXs+1:end);




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Jacobian  &  L.I columns in System Q(Y)X=q  &  List FixVars
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[fixedEqs,fixedVars,Qsub,QQ,varQ] = FixVars(F,size(Prob.A,1),NoXs,varXY,numXY,tol);

newF = subs(F,fixedVars,(fixedVars-fixedEqs));


varX = (jacobian(subs(varX,fixedVars,(fixedVars-fixedEqs)),varX)*varX')';
varX(varX==0)=[];
numX = numX(logical(diag(jacobian(subs(varX,fixedVars,(fixedVars-fixedEqs)),varX))));
varXY = [varX, varY];
numXY = [numX, numY];


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Substitutions and fixed variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

jacF= jacobian(newF, varXY);

varXmatsubs = subs(varXmatsubs,fixedVars,(fixedVars-fixedEqs));




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Critical Point Method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
varL = sym('L', [1 length(newF)]);
assume(varL,'real');

% Direction of projection: deriv respect x1 or random vector
jacQ = jacobian(newF, varXY);
projVec = randi([-100,100],length(newF),1);


CPM = [ newF ; varL*projVec-1];

for i=2:size(jacQ,2)
    CPM = [CPM ; varL*jacQ(:,i)];
end  
CPM(CPM==0)=[];


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Save polynomial system in text file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Projecting on Xs vars!')
GenElimIdeal(CPM , varX );% This funciton needs maple.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% RUN Exact Solver
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if ispc
    eval('!maple.bat ');%WINDOWS
elseif isunix
    disp('Running msolve!')
    eval('!msolve -f EQS/EQSmsolve.txt -o EQS/msolveOUTPUT')
    disp('finished!!')
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% CHECK msolve SOLUTIONS Numerically
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('Checking MSOLVE solutions!')

CheckSDPSolution(varX,varY,varL,varXmatsubs)

